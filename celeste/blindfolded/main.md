# Celeste Blindfolded strategies (any%)

## What?

This is documentation for blindfolded Celeste strats & routing.

## Termonology

- All the standard termonology for Madeline's moveset will likely be used at some point.
<!-- - short direction names are:
  - up: `u`
  - down: `d`
  - left: `l`
  - right: `r`
  - up-left: `ul`
  - up-right: `ur`
  - down-left: `dl`
  - down-right: `dr` -->

## Sections by chapter.

### Prologue
[Prologue](./prologue.md)

### Chapter 1

  - [Chapter 1A](./1a.md)
  - [Chapter 1B](./1b.md)
  - [Chapter 1C](./1c.md2)
### Chapter 2
  - [Chapter 2A](./2a.md)
  - [Chapter 2B](./2b.md)
  - [Chapter 2C](./2c.md)
### Chapter 3
  - [Chapter 3A](./3a.md)
  - [Chapter 3B](./3b.md)
  - [Chapter 3C](./3c.md)
### Chapter 4
  - [Chapter 4A](./4a.md)
  - [Chapter 4B](./4b.md)
  - [Chapter 4C](./4c.md)
### Chapter 5
  - [Chapter 5A](./5a.md)
  - [Chapter 5B](./5b.md)
  - [Chapter 5C](./5c.md)
### Chapter 6
  - [Chapter 6A](./6a.md)
  - [Chapter 6B](./6b.md)
  - [Chapter 6C](./6c.md)
### Chapter 7
  - [Chapter 7A](./7a.md)
  - [Chapter 7B](./7b.md)
  - [Chapter 7C](./7c.md)
### Chapter 8
  - [Chapter 8A](./8a.md)
  - [Chapter 8B](./8b.md)
  - [Chapter 8C](./8c.md)
### Farewell

[Farewell](./farewell.md)

